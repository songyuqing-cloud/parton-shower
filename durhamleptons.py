
import math as m

from vector import Vec4
from particle import Particle
import sys 

class Algorithm:

    def Yij(self,p,q):
        pq = p.px*q.px+p.py*q.py+p.pz*q.pz
        return 2.*pow(min(p.E,q.E),2)*(1.0-min(max(pq/m.sqrt(p.P2()*q.P2()),-1.0),1.0))/self.ecm2

    def lepton_number(self,particle):
        id = particle.pid 
        if id > 0:
            if id == 11 or id == 13 or id == 15:
                return 1 
            else: 
                return 0
        else: 
            if id == -11 or id == -13 or id == -15:
                return -1 
            else: 
                return 0

    def Cluster(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        p = [ i.mom for i in event[2:] ]
        kt2 = []
        n = len(p)
        imap = range(n)
        kt2ij = [ [ 0 for i in range(n) ] for j in range(n) ]
        dmin = 1
        # edit to keep track of what gets clustered and where the total 4-momentum is 
        # is this what imap does? 
        for i in range(n):
            for j in range(i):
                dij = kt2ij[i][j] = self.Yij(p[i],p[j])
                if dij < dmin: dmin = dij; ii = i; jj = j
        while n>2:
            n -= 1
            kt2.append(dmin)
            jjx = imap[jj]
            p[jjx] += p[imap[ii]]
            for i in range(ii,n): imap[i] = imap[i+1]
            for j in range(jj): kt2ij[jjx][imap[j]] = self.Yij(p[jjx],p[imap[j]])
            for i in range(jj+1,n): kt2ij[imap[i]][jjx] = self.Yij(p[jjx],p[imap[i]])
            dmin = 1
            for i in range(n):
                for j in range(i):
                    dij = kt2ij[imap[i]][imap[j]]
                    if dij < dmin: dmin = dij; ii = i; jj = j
        
        return imap, p 

    def Dress_Leptons(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        event_copy = [ p.Copy() for p in event ]
        dmax = 0.1 
        for i in event_copy[2:]:
            if self.lepton_number(i) != 0:
                for j in event_copy[2:]:
                    if j.pid == 22:
                        if self.Yij(i.mom,j.mom) < dmax:
                            i.mom += j.mom 
                            event_copy.remove(j)
        return event_copy 


    # apply isolation criteria 
    def Isolated_Leptons(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        event_dressed = self.Dress_Leptons(event)
        imap, p = self.Cluster(event_dressed)
        iso_lepton_is = []
        iso_leptons = []
        for index,i in enumerate(event_dressed[2:]):
            if self.lepton_number(i) != 0:
                if i.mom[0] < 1: continue # too low energy 
                jjx = imap[index]
                Etot = 0 
                jet_lepton_number = self.lepton_number(i)
                for j in range(jjx):
                    if j != index:
                        if imap[j] == jjx:
                            Etot += event_dressed[2+j].mom[0]
                            jet_lepton_number += self.lepton_number(event_dressed[2+j])

                if Etot > max(1,0.1*i.mom[0]): continue # not mostly lepton 
                if jet_lepton_number == 0: continue 
                # all criteria met 
                iso_leptons.append(i)
                iso_lepton_is.append(index)
        return iso_lepton_is, iso_leptons

    def dRjet(self,event,isolated):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        event_dressed = self.Dress_Leptons(event)
        imap, p = self.Cluster(event_dressed)
        if isolated:
            ileptons,_ = self.Isolated_Leptons(event)
        else: 
            ileptons = [ i for i,l in enumerate(event_dressed[2:]) if self.lepton_number(l) != 0 ]
        
        dists = []
        for i in ileptons:
            jjx = imap[i]
            dij = [] 
            for j in range(len(imap)):
                if imap[j] == jjx: continue # same jet 
                dij.append(self.Yij(event_dressed[2+i].mom,p[imap[j]]))
            dists.append(max(min(dij),1e-06))

        if len(dists) > 0:
            dists.sort(reverse=True) # descending order 
        return dists 

    def dRlepton(self,event,isolated):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        event_dressed = self.Dress_Leptons(event)
        if isolated:
            _,leptons = self.Isolated_Leptons(event)
        else: 
            leptons = [ l for l in event_dressed[2:] if self.lepton_number(l) != 0 ]
        dists = []
        for i in range(len(leptons)):
            for j in range(i+1,len(leptons)):
                if leptons[j].pid == -leptons[i].pid:
                    yij = self.Yij(leptons[i].mom,leptons[j].mom)
                    dists.append(max(yij,1e-06))

        if len(dists) > 0:
            dists.sort(reverse=True) # descending order 
        return dists 


from histogram import Histo1D

class Analysis:

    def __init__(self,n=1):
        self.n = 0.
        self.ynm = [[ Histo1D(100,-4.3,-0.3,'/LL_LeptonRates/log10_dR_jet_{}\n'.format(i+1))
                      for i in range(4) ] for i in range(n+1) ]
        self.duralg = Algorithm()
        self.iso = True # change for isolated or not 

    def Analyze(self,event,w,weights=[]):
        self.n += 1.
        # change for dRlepton or dRjet 
        kt2 = self.duralg.dRjet(event,self.iso) 
        for j in range(len(self.ynm[0])):
            self.ynm[0][j].Fill(m.log10(kt2[j]) if len(kt2)>j else -5.,w)

    def Finalize(self,name):
        for h in self.ynm[0]: h.ScaleW(1./self.n)
        isoString = "-iso" if self.iso else ""
        file = open(name+isoString+".yoda","w")
        file.write("\n\n".join([ str(h) for h in self.ynm[0] ]))
        file.close() 
