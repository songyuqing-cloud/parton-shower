
import math as m

from vector import Vec4
from particle import Particle

class Algorithm:

    def Yij(self,p,q):
        pq = p.px*q.px+p.py*q.py+p.pz*q.pz
        return 2.*pow(min(p.E,q.E),2)*(1.0-min(max(pq/m.sqrt(p.P2()*q.P2()),-1.0),1.0))/self.ecm2

    def lepton_number(self,particle):
        id = particle.pid 
        if id > 0:
            if id == 11 or id == 13 or id == 15:
                return 1 
            else: 
                return 0
        else: 
            if id == -11 or id == -13 or id == -15:
                return -1 
            else: 
                return 0

    def Cluster(self,event):
        self.ecm2 = (event[0].mom+event[1].mom).M2()
        p = [ i.mom for i in event[2:] ]
        E = []
        n = len(p)

        for i,particle in enumerate(event[2:]):
            if self.lepton_number(particle) != 0:
                E.append(p[i][0])

        if len(E) > 0:
            E.sort(reverse=True) # descending order 
        return E
        

from histogram import Histo1D

class Analysis:

    def __init__(self,n=1):
        self.n = 0.
        self.ynm = [ Histo1D(100,-1.,3.,'/LL_LeptonRates/lepton_kT_{}\n'.format(i+1))
                      for i in range(2) ]
        self.duralg = Algorithm()

    def Analyze(self,event,w,weights=[]):
        self.n += 1.
        kt2s = self.duralg.Cluster(event)
        for j in range(len(self.ynm)):
            self.ynm[j].Fill(m.log10(kt2s[j]) if len(kt2s)>j else -5.,w)

    def Finalize(self,name):
        for h in self.ynm: h.ScaleW(1./self.n)
        file = open(name+".yoda","w")
        file.write("\n\n".join([ str(h) for h in self.ynm ]))
        file.close() 