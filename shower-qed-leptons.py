import math as m
import random as r
from vector import Vec4 
from particle import Particle, CheckEvent
from qcd import AlphaS #, NC, TR, CA, CF

alpha_qed = 1./128.802  # should this actually be alpha_max? 
NC = 1
TR = 0.5 # should this be 1?
CA = 0
CF = 2*TR*NC 

class Kernel:
    def __init__(self,flavs,charge):
        self.flavs = flavs
        self.charge = charge 
        self.weightSign = 1

class Pqq (Kernel):
    # quark -> quark (p_i) + photon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 10. 

    def Value(self,z,y,t):
        # eq. 3.22a in arXiv:1411.4085 
        return self.Weight(t) * self.charge**2 * CF*(2./(1.-z*(1.-y))-(1.+z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * self.charge**2 * CF*2./(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * self.charge**2 * CF*2.*m.log((1.-zm)/(1.-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random())

class Pgq (Kernel):
    # photon -> quark (p_i) + antiquark (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)
    
    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 10.

    def Value(self,z,y,t):
        # eq. 1.3c
        return self.Weight(t) * self.charge**2 * TR/2*(z*z + (1-z)*(1-z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * self.charge**2 * TR/2

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * self.charge**2 * TR/2 * (zp-zm)

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return zm+(zp-zm)*r.random()


class Shower:

    def __init__(self,alpha,t0):
        self.t0 = t0 # cutoff scale
        self.alpha = alpha_qed # qed coupling 
        self.alphamax = alpha_qed
        # all relevant quark flavours 
        self.kernels = [ Pqq([fl,fl,22],-1./3.) for fl in [1,3,5] ]
        self.kernels += [ Pqq([fl,fl,22],1./3.) for fl in [-1,-3,-5] ]
        self.kernels += [ Pqq([fl,fl,22],2./3.) for fl in [2,4] ]
        self.kernels += [ Pqq([fl,fl,22],-2./3.) for fl in [-2,-4] ]
        self.kernels += [ Pgq([22,fl,-fl],1./3.) for fl in [1,3,5] ]
        self.kernels += [ Pgq([22,fl,-fl],2./3.) for fl in [2,4] ]

        # leptons 
        self.kernels += [ Pqq([fl,fl,22],-1.) for fl in [11,13,15] ] # tau treated as massless since b is 
        self.kernels += [ Pqq([fl,fl,22],1.) for fl in [-11,-13,-15] ] 
        self.kernels += [ Pgq([22,fl,-fl],1) for fl in [11,13,15] ] 

        self.eventweight = 1.

    def MakeKinematics(self,z,y,phi,pijt,pkt):
        # pijt=p_ij(t) is the emitter momentum before branching 
        # pkt=p_k(t) is the spectator momentum before branching 
        # rkt^2 = 2 pijt.pkt y z (1-z)
        Q = pijt + pkt # total momentum before splitting 
        rkt = m.sqrt(Q.M2() *y*z*(1.-z)) # magnitude of transverse momentum 
                                        #(if emitter and spectator are massless)
        kt1 = pijt.Cross(pkt) # a transverse momentum 
        if kt1.P() < 1.e-6: # if pij and pk are collinear 
            kt1 = pijt.Cross(Vec4(0.,1.,0.,0.)) # perpendicular to x direction 
        kt1 *= rkt * m.cos(phi) / kt1.P() # correct magnitude, add random phase 

        kt2cms = Q.Boost(pijt).Cross(kt1)
        kt2cms *= rkt * m.sin(phi) / kt2cms.P() 
        kt2 = Q.BoostBack(kt2cms)

        pi = z*pijt + (1.-z)*y*pkt + kt1 + kt2 
        pj = (1.-z)*pijt + z*y*pkt - kt1 - kt2 
        pk = (1.-y)*pkt 

        return [pi,pj,pk]

    # photons are colourless 
    # photon must have colour [0,0]
    # must split into [c,0] and [0,c] pair 
    # q or qbar colour unchanged if it emits a photon
    def MakeColors(self,flavs,colij,colk):
        # self.c += 1 
        if flavs[0] != 21 and flavs[0] != 22: # if quark splits
            if 0 < flavs[0] < 8: # quark
                return [ [colij[0],0], [0,0] ]
            elif -8 < flavs[0] < 0: # antiquark 
                return [ [0,colij[1]], [0,0] ]
            elif 10 < flavs[0] < 16:
                return [0,0] # lepton 
            elif -16 < flavs[0] < -10:
                return [0,0] # antilepton 
        else:
            if flavs[0] == 22: 
                    return [ [self.c,0], [0,self.c] ]
            if flavs[0] != 21: 
                return [0,0] # lepton 

    def GeneratePoint(self,event):
        while self.t > self.t0:
            t = self.t0 # comparing value
            # iterate over all splitter-spectator pairs 
            for split in event[2:]:
                for spect in event[2:]:
                    if spect == split:
                        continue # move on to next item 

                    # compute charge correlator 
                    Qsq = 0 
                    if split.pid != 22 and split.pid != 21: # quark or lepton 
                        Qsq = split.EMCharge() * spect.EMCharge() / split.EMCharge()**2 
                    elif split.pid == 22: # photon 
                        Qsq = -1./(len(event[2:])-1) # all other FS particles given equal weight 

                    if Qsq == 0:
                        continue 

                    # iterate over all kernels that can branch the splitter 
                    for sf in self.kernels: 
                        if sf.flavs[0] != split.pid:
                            continue 

                        sf.weightSign = Qsq/abs(Qsq)
                        # compute z boundaries 
                        # t = |k_T|^2
                        # min and max z are roots of this eqn 
                        m2 = (split.mom + spect.mom).M2()
                        if m2 < 4*self.t0:
                            continue # no allowed z 
                        zp = 0.5*(1+m.sqrt(1-4*self.t0/m2))
                        # print(zp) # debugging 
                        # trial emission, overestimating alpha and using splitting function overestimate 
                        g = self.alphamax/(2*m.pi) * sf.Integral(1.-zp,zp,self.t) * Qsq # must be positive 
                        tt = self.t * m.pow(r.random(),1./g)
                        # take the higher ordering variable and store data 
                        if tt > t:
                            t = tt 
                            s = [split, spect, sf, m2, zp, Qsq]
            self.t = t
            # if 'winner' was found, generate a value for z 
            if t > self.t0:
                z = s[2].GenerateZ(1.-s[4],s[4])
                # calculate y from z and t
                y = t/s[3]/z/(1.-z)
                if y < 1: 
                    # accept/reject procedure: compare f(z)/g(z) to a random number 
                    # extra factor 1-y is the phase-space factor. eq. 5.20
                    # f/g is the same regardless of weight 
                    # h/g = weight 
                    f = (1.-y) * self.alpha * s[2].Value(z,y,t) * s[5] / s[2].Weight(t)
                    g = self.alphamax * s[2].Estimate(z,t) * s[5] / s[2].Weight(t)
                    if g==0:
                        print("\n")
                        print(s[5])
                        print(event)
                        print(s[0],s[1])
                        print(s[0].EMCharge() * s[1].EMCharge() / s[0].EMCharge()**2 )
                    h = self.alphamax * s[2].Estimate(z,t) 

                    if f/g > r.random():
                        # splitting happened!
                        self.eventweight *= g/h # analytic reweighting
                        phi = 2*m.pi*r.random() # random azimuthal angle 
                        moms = self.MakeKinematics(z,y,phi,s[0].mom,s[1].mom)
                        cols = self.MakeColors(s[2].flavs,s[0].col,s[1].col)

                        # for charge conservation
                        eChargeBefore = 0
                        for p in event: 
                            eChargeBefore += p.EMCharge()

                        event.append(Particle(s[2].flavs[2],moms[1],cols[1])) 
                        # splitter and spectator get new kinematics, flavour and colour 
                        s[0].Set(s[2].flavs[1],moms[0],cols[0])
                        s[1].mom = moms[2]

                        # check charge conservation 
                        eChargeAfter = 0
                        for p in event: 
                            eChargeAfter += p.EMCharge()
                        if eChargeAfter != eChargeBefore:
                            print("\nCharge not conserved")
                            print("{} != {}".format(eChargeBefore,eChargeAfter))

                        return 
                    else: 
                        self.eventweight *= g/h * (h-f)/(g-f) # analytic reweighting 

    def Run(self,event,weight,t):
        self.c = 1
        self.t = t
        self.eventweight = weight 
        while self.t > self.t0: 
            self.GeneratePoint(event)

# test program 
# build and run the generator

import sys
from matrix import eetojj
from durham import Analysis

alphas = AlphaS(91.1876,0.118) # set running coupling: alpha_s(Mz)=0.118
hardxs = eetojj(alphas)
shower = Shower(alphas,t0=1.) # cutoff scale = 1 GeV
jetrat = Analysis()

r.seed(123456)
for i in range(1000000): # no of events generated 
    event, weight = hardxs.GenerateLOPoint()
    t = (event[0].mom+event[1].mom).M2()
    shower.Run(event,weight,t)
    finalweight = shower.eventweight
    sys.stdout.write('\rEvent {0}'.format(i))
    sys.stdout.flush()
    jetrat.Analyze(event,finalweight) # Durham jet algorithm analysis 
jetrat.Finalize("qedshower-weighted")
print("")
