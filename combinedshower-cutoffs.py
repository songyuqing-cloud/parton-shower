import math as m
import random as r
from vector import Vec4 
from particle import Particle, CheckEvent
from qcd import AlphaS, NC, TR, CA, CF

alpha_qed = 1./137.035999

class Kernel:
    def __init__(self,flavs,charge,qed,cutoff):
        self.flavs = flavs
        self.charge = charge 
        self.weightSign = 1
        self.qed = qed
        self.cutoff = cutoff

class Pqqg (Kernel):
    # quark -> quark (p_i) + gluon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 1. 

    def Value(self,z,y,t):
        # eq. 3.22a in arXiv:1411.4085 
        return self.Weight(t) * CF*(2./(1.-z*(1.-y))-(1.+z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * CF*2./(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * CF*2.*m.log((1.-zm)/(1.-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random())

class Pqq (Kernel):
    # quark/lepton -> quark/lepton (p_i) + photon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 10. 

    def Value(self,z,y,t):
        # eq. 3.22a in arXiv:1411.4085 
        return self.Weight(t) * self.charge**2 * (2./(1.-z*(1.-y))-(1.+z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * self.charge**2 * 2./(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * self.charge**2 * 2.*m.log((1.-zm)/(1.-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random())

class Ppq (Kernel):
    # photon -> quark (p_i) + antiquark (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)
    
    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        # if self.flavs[1] == 11 or self.flavs[1] == 13 or self.flavs[1] == 15:
        #     return self.weightSign * 100.
        return self.weightSign * 1.

    def Value(self,z,y,t):
        # eq. 1.3c
        return self.Weight(t) * self.charge**2 * (z*z + (1-z)*(1-z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * self.charge**2 

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * self.charge**2 * (zp-zm)

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return zm+(zp-zm)*r.random()

class Pgq (Kernel):
    # gluon -> quark (p_i) + antiquark (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)
    
    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 1.

    def Value(self,z,y,t):
        # eq. 1.3c
        return self.Weight(t) * TR/2*(z*z + (1-z)*(1-z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * TR/2

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * TR/2 * (zp-zm)

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return zm+(zp-zm)*r.random()

class Pgg (Kernel):
    # gluon -> gluon (p_i) + gluon (p_j)
    # spectator momentum p_k 
    # z = p_i p_k / (p_i p_k + p_j p_k)
    # y = p_i p_j / (p_i p_j + p_i p_k + p_j p_k)

    def Weight(self,t):
        # arbitrary weight h(t)/g(t)
        return self.weightSign * 1.

    def Value(self,z,y,t):
        # eq. 3.22b in arXiv:1411.4085 
        return self.Weight(t) * CA/2*(2/(1.-z*(1.-y)) - 2 + z*(1.-z))

    def Estimate(self,z,t):
        # overestimate for veto algorithm 
        return self.Weight(t) * CA*1/(1.-z)

    def Integral(self,zm,zp,t):
        # integral of estimate above between zm and zp 
        return self.Weight(t) * CA*m.log((1-zm)/(1-zp))

    def GenerateZ(self,zm,zp):
        # selects a value of z between zm and zp 
        # according to probability dist Estimate 
        return 1.+(zp-1.)*m.pow((1.-zm)/(1.-zp),r.random()) 

class Shower:

    def __init__(self,alpha,t0,mur2fac=1.):
        self.t0 = t0 # cutoff scale
        self.t0min = 0.01 # photon emission cutoff 
        self.alphas = alpha
        self.alphasmax = alpha(self.t0)
        self.alpha = alpha_qed # qed coupling 
        self.alphamax = alpha_qed

        # qcd 
        self.kernels = [ Pqqg([fl,fl,21],-1./3.,0,t0) for fl in [1,3,5] ]
        self.kernels += [ Pqqg([fl,fl,21],1./3.,0,t0) for fl in [-1,-3,-5] ]
        self.kernels += [ Pqqg([fl,fl,21],2./3.,0,t0) for fl in [2,4] ]
        self.kernels += [ Pqqg([fl,fl,21],-2./3.,0,t0) for fl in [-2,-4] ]
        self.kernels += [ Pgq([21,fl,-fl],1./3.,0,t0) for fl in [1,3,5] ]
        self.kernels += [ Pgq([21,fl,-fl],2./3.,0,t0) for fl in [2,4] ]
        # self.kernels += [ Pgq([21,fl,-fl],1./3.,0,max(t0,2*self.Mass(fl))) for fl in [1,3,5] ]
        # self.kernels += [ Pgq([21,fl,-fl],2./3.,0,max(t0,2*self.Mass(fl))) for fl in [2,4] ]
        self.kernels += [ Pgg([21,21,21],0,0,t0) ]
        # note charge will not be used 

        # qed
        # quarks
        self.kernels += [ Pqq([fl,fl,22],-1./3.,1,self.t0min) for fl in [1,3,5] ] 
        self.kernels += [ Pqq([fl,fl,22],1./3.,1,self.t0min) for fl in [-1,-3,-5] ]
        self.kernels += [ Pqq([fl,fl,22],2./3.,1,self.t0min) for fl in [2,4] ]
        self.kernels += [ Pqq([fl,fl,22],-2./3.,1,self.t0min) for fl in [-2,-4] ]
        # self.kernels += [ Ppq([22,fl,-fl],1./3.,1,t0) for fl in [1,3,5] ]
        # self.kernels += [ Ppq([22,fl,-fl],2./3.,1,t0) for fl in [2,4] ]
        # self.kernels += [ Ppq([22,fl,-fl],1./3.,1,max(self.t0min,2*self.Mass(fl))) for fl in [1,3,5] ]
        # self.kernels += [ Ppq([22,fl,-fl],2./3.,1,max(self.t0min,2*self.Mass(fl))) for fl in [2,4] ]
        # leptons 
        self.kernels += [ Pqq([fl,fl,22],-1.,1,self.t0min) for fl in [11,13,15] ] # tau treated as massless since b is 
        self.kernels += [ Pqq([fl,fl,22],1.,1,self.t0min) for fl in [-11,-13,-15] ] 
        # self.kernels += [ Ppq([22,fl,-fl],1,1,t0) for fl in [11,13,15] ] 
        # self.kernels += [ Ppq([22,fl,-fl],1,1,max(self.t0min,2*self.Mass(fl))) for fl in [11,13,15] ] 

        self.eventweight = 1.
        self.mur2facs = [1./mur2fac,mur2fac] if mur2fac != 1. else []

    def Reweight(self,parton,t,g,h,accept):
        for i in range(len(self.mur2facs)):
            f = g*self.alphas(self.mur2facs[i]*t)/self.alphas(t)
            parton.AddWeight(i,t,f/g if accept else (h-f)/(h-g))

    def AddWeight(self,event,t):
        for parton in event:
            parton.GetWeight(self.w,t)
            parton.wgt = []

    def Mass(self,fl):
        if abs(fl) == 11: return 0.0005
        if abs(fl) == 13: return 0.2 
        if abs(fl) == 15: return 1.8
        if abs(fl) == 1: return 0.005
        if abs(fl) == 2: return 0.003
        if abs(fl) == 3: return 0.1
        if abs(fl) == 4: return 1.3
        if abs(fl) == 5: return 4.2
        return 0

    def MakeKinematics(self,z,y,phi,pijt,pkt):
        # pijt=p_ij(t) is the emitter momentum before branching 
        # pkt=p_k(t) is the spectator momentum before branching 
        # rkt^2 = 2 pijt.pkt y z (1-z)
        Q = pijt + pkt # total momentum before splitting 
        rkt = m.sqrt(Q.M2() *y*z*(1.-z)) # magnitude of transverse momentum 
                                        #(if emitter and spectator are massless)
        kt1 = pijt.Cross(pkt) # a transverse momentum 
        if kt1.P() < 1.e-6: # if pij and pk are collinear 
            kt1 = pijt.Cross(Vec4(0.,1.,0.,0.)) # perpendicular to x direction 
        kt1 *= rkt * m.cos(phi) / kt1.P() # correct magnitude, add random phase 

        kt2cms = Q.Boost(pijt).Cross(kt1)
        kt2cms *= rkt * m.sin(phi) / kt2cms.P() 
        kt2 = Q.BoostBack(kt2cms)

        pi = z*pijt + (1.-z)*y*pkt + kt1 + kt2 
        pj = (1.-z)*pijt + z*y*pkt - kt1 - kt2 
        pk = (1.-y)*pkt 

        return [pi,pj,pk]

    # photons are colourless 
    # photon must have colour [0,0]
    # must split into [c,0] and [0,c] pair 
    # q or qbar colour unchanged if it emits a photon
    def MakeColors(self,flavs,colij,colk):
        self.c += 1 

        if flavs[0] != 21 and flavs[0] != 22: # if quark or lepton splits

            if flavs[2] == 22: # emits photon 
                if flavs[0] > 0: # quark or lepton 
                    return [ [colij[0],0], [0,0] ]
                else: # antiquark or lepton 
                    return [ [0,colij[1]], [0,0] ]
            
            else: # emits gluon
                if flavs[0] > 0: # quark 
                    return [ [self.c,0], [colij[0],self.c] ]
                else: # antiquark 
                    return [ [0,self.c], [self.c,colij[1]] ]

        elif flavs[0] == 22: # photon splits 

            if 0 < flavs[1] < 8: # quark-antiquark
                return [ [self.c,0], [0,self.c] ]
            if -8 < flavs[1] < 0: # antiquark-quark 
                return [ [0,self.c], [self.c,0] ]
            else: # lepton-antilepton
                return [ [0,0], [0,0] ]

        else: # gluon splits. MAKE SURE NOT INTO LEPTONS 

            if flavs[1] == 21: # g->gg
                if colij[0] == colk[1]:
                    if colij[1] == colk[0] and r.random()>0.5:
                        return [ [colij[0],self.c], [self.c,colij[1]] ]
                    else:
                        return [ [self.c,colij[1]], [colij[0],self.c] ]
                else:
                    return [ [colij[0],self.c], [self.c,colij[1]] ]
            
            else: # g->qq
                if flavs[1] > 0:
                    return [ [colij[0],0], [0,colij[1]] ]
                else:
                    return [ [0,colij[1]], [colij[0],0] ]


    def GeneratePoint(self,event):
        while self.t > self.t0min:
            t = self.t0min # comparing value
            # iterate over all splitter-spectator pairs 
            for split in event[2:]:
                for spect in event[2:]:
                    if spect == split:
                        continue # move on to next item 

                    # iterate over all kernels that can branch the splitter 
                    for sf in self.kernels: 
                        if sf.flavs[0] != split.pid:
                            continue 

                        if self.t < sf.cutoff: 
                            continue 

                        # print("self.t = {}".format(self.t))
                        # print("t = {}".format(t))

                        if sf.qed:
                            # compute charge correlator 
                            Qsq = 0 
                            if split.pid != 22 and split.pid != 21: # quark or lepton 
                                Qsq = split.EMCharge() * spect.EMCharge() / split.EMCharge()**2 
                            elif split.pid == 22: # photon 
                                Qsq = -1./(len(event[2:])-1) # all other FS particles given equal weight 

                            if Qsq == 0:
                                continue 
                        
                            sf.weightSign = Qsq/abs(Qsq)

                        else:
                            Qsq = 1
                            if not split.ColorConnected(spect):
                                continue 

                        # compute z boundaries 
                        # t = |k_T|^2
                        # min and max z are roots of this eqn 
                        m2 = (split.mom + spect.mom).M2()
                        if m2 < 4*sf.cutoff:
                            continue # no allowed z 
                        zp = 0.5*(1+m.sqrt(1-4*sf.cutoff/m2))
                        
                        # which coupling? 
                        if sf.qed: alpha_max = self.alphamax
                        else: alpha_max = self.alphasmax

                        # trial emission, overestimating alpha and using splitting function overestimate 
                        g = alpha_max/(2*m.pi) * sf.Integral(1.-zp,zp,self.t) * Qsq # must be positive 
                        tt = self.t * m.pow(r.random(),1./g)
                        # if t < sf.cutoff:
                        #     t = sf.cutoff
                        # take the higher ordering variable and store data 
                        if tt > t:
                            t = max(tt,sf.cutoff)
                            s = [split, spect, sf, m2, zp, Qsq]
            self.t = t
            # if 'winner' was found, generate a value for z 
            if t > self.t0min:
                z = s[2].GenerateZ(1.-s[4],s[4])
                # calculate y from z and t
                y = t/s[3]/z/(1.-z)
                if y < 1: 
                    # accept/reject procedure: compare f(z)/g(z) to a random number 
                    # extra factor 1-y is the phase-space factor. eq. 5.20
                    # f/g is the same regardless of weight 
                    # h/g = weight 

                    # which coupling? 
                    if s[2].qed: 
                        a = self.alpha
                        alpha_max = self.alphamax
                    else: 
                        a = self.alphas(t)
                        alpha_max = self.alphasmax
                    # consistency check: is the probability of splitting the correct ratio for QCD/QED? 

                    f = (1.-y) * a * s[2].Value(z,y,t) * s[5] / s[2].Weight(t)
                    g = alpha_max * s[2].Estimate(z,t) * s[5] / s[2].Weight(t)
                    h = alpha_max * s[2].Estimate(z,t) * s[5] * s[2].weightSign 

                    if f/g > r.random():
                        # splitting happened!
                        
                        phi = 2*m.pi*r.random() # random azimuthal angle 
                        moms = self.MakeKinematics(z,y,phi,s[0].mom,s[1].mom)
                        cols = self.MakeColors(s[2].flavs,s[0].col,s[1].col)

                        # for charge conservation
                        eChargeBefore = 0
                        for p in event: 
                            eChargeBefore += p.EMCharge()

                        event.append(Particle(s[2].flavs[2],moms[1],cols[1])) 
                        # splitter and spectator get new kinematics, flavour and colour 
                        s[0].Set(s[2].flavs[1],moms[0],cols[0])
                        s[1].mom = moms[2]

                        # check charge conservation 
                        eChargeAfter = 0
                        for p in event: 
                            eChargeAfter += p.EMCharge()
                        if eChargeAfter < eChargeBefore - 1e-15 or eChargeAfter > eChargeBefore + 1e-15:
                            print("\nCharge not conserved")
                            print("{} != {}".format(eChargeBefore,eChargeAfter))
                            print(event)

                        self.eventweight *= g/h # analytic reweighting
                        if not s[2].qed:
                            self.Reweight(s[0],t,f,g,1) # parton reweighting for renormalisation scale variation 
                        return 
                    else: 
                        self.eventweight *= g/h * (h-f)/(g-f) # analytic reweighting 
                        if not s[2].qed:
                            self.Reweight(s[0],t,f,g,0) # parton reweighting for renormalisation scale variation 

    def Run(self,event,weight,t):
        self.c = 1
        self.t = t
        self.eventweight = weight 
        self.w = [ 1. for mur2fac in self.mur2facs ]
        while self.t > self.t0: # bug? should this be self.t0min? Test 
            self.GeneratePoint(event)
            self.AddWeight(event,self.t)
        self.AddWeight(event,self.t0) # weights in self.w. Should this be self.t0min?

# test program 
# build and run the generator

import sys
from matrix import eetojj
from durham import Analysis

alphas = AlphaS(91.1876,0.118) # set running coupling: alpha_s(Mz)=0.118
hardxs = eetojj(alphas)
shower = Shower(alphas,t0=1.,mur2fac=1.) # cutoff scale = 1 GeV
jetrat = Analysis(len(shower.mur2facs))

r.seed(123456)
nevents = 100000
for i in range(nevents): # no of events generated 
    event, weight = hardxs.GenerateLOPoint()
    t = (event[0].mom+event[1].mom).M2()
    shower.Run(event,weight,t)
    finalweight = shower.eventweight
    sys.stdout.write('\rEvent {0}'.format(i))
    sys.stdout.flush()
    jetrat.Analyze(event,finalweight,shower.w) # Durham jet algorithm analysis 
jetrat.Finalize("qcdcutoffs-{0}_{1}".format(shower.t0min,nevents))
print("")
